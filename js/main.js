// Hệ thống quản lí nhân viên
// Người tạo: Đinh Quốc Đạt
// Chức năng:
// 1. Cho phép thêm nhân viên vào trong hệ thống
// 2. Hiển thị danh sách toàn bộ nhân viên ra màn hình
// 3. Xóa nhân viên khỏi hệ thống
// 4. Cập nhật thông tin của 1 nhân viên
// 5. Tìm kiếm nhân viên theo tên hoặc mã nhân viên
// 6. Validation dữ liệu

//Global
var empList = [];
//Funnction 2: Tìm nhân viên by ID
const findEmpByID = function (id) {
  for (var index = 0; index < empList.length; index++) {
    if (empList[index].id === id) {
      return index;
    }
  }
  return -1;
};

//Function 1: Thêm nhân viên
const addEmp = function () {
  //input
  const lastName = document.getElementById("ho").value;
  const firstName = document.getElementById("ten").value;
  const id = document.getElementById("msnv").value;
  const startDate = document.getElementById("datepicker").value;
  const position = document.getElementById("chucvu").value;

  var isValid = true;

  // check validation
  isValid = isValid & checkRequired('ho','lastNameError', "*Vui lòng nhập họ");
  isValid = isValid & checkRequired('ten','firstNameError', "*Vui lòng nhập tên");
  isValid = isValid & checkRequired('msnv','idError', "*Vui lòng nhập mã nhân viên");
  isValid = isValid & checkRequired('chucvu','positionError', "*Vui lòng chọn chức vụ");

  if (isValid === 0){
    return;
  }

    //Check id trùng trong list
    const check = findEmpByID(id);

    if (check !== -1) {
      alert("ID của nhân viên đã tồn tại");
      return;
    }
    const newEmp = new Employee(id, lastName, firstName, startDate, position);
    empList.push(newEmp);
    //Render giao diện
    renderEmp();
    console.log(empList);

    //SaveData in Local Storage
    saveData();
};

//Thêm sự kiện cho button trong js
//Callback function
//Hàm đóng vai trò là 1 tham số đầu vào trong 1 hàm khác thì gọi là callback function
// document.getElementById("addNew").addEventListener("click",addEmp);
const renderEmp = function (arr) {
  //nếu có truyền arr, thì dựa vào arr mà render giao diện
  //nếu không, thì mặc định là empList
  arr = arr || empList;
  //cú pháp Hoặc A Hoăc B , nếu A đúng thì lấy A, nếu A sai thì qua check B,
  //B đúng hay sai gì cũng lấy, thì là lựa chọn cuối

  //Dấu `` (template string) cho phép bọc chuỗi và viết xuống dòng
  //Cú pháp offer thêm biến vào chuỗi ${}
  var htmlContent = "";
  for (var index = 0; index < arr.length; index++) {
    const currentEmp = arr[index];
    htmlContent += `
		<tr>
		 	<td>${index + 1}</td>
		 	<td>${currentEmp.lastName + " " + currentEmp.firstName}</td>
		 	<td>${currentEmp.id}</td>
		 	<td>${currentEmp.startDate}</td>
		 	<td>${currentEmp.position}</td>
			<td>${currentEmp.calcSalary()}</td>
			<td>
				<button class="btn btn-info rounded-circle" onClick="getUpdateEmp('${
          currentEmp.id
        }')">
					<i class="fa fa-edit"></i>
				</button>
				<button class="btn btn-danger rounded-circle" onClick="deleteEmp('${
          currentEmp.id
        }')">
					<i class="fa fa-trash"></i>
				</button>
			</td> 
		</tr>`;
  }
  document.getElementById("tbodyEmp").innerHTML = htmlContent;
};

//function 3: Save data in Local Storage/Section Storage/Cookie
const saveData = function () {
  // setItem('Tên','loại gì');
  //Cast mảng -> chuỗi: JSON.stringify(Array);
  localStorage.setItem("empList", JSON.stringify(empList));
};

//function 4: LoadData in Local Storage
const getData = function () {
  var empListJson = localStorage.getItem("empList");
  if (!empListJson) {
    return;
  }
  //Map từ bảng cũ [n1,n2,n3] -> [new Emp(1), new Emp(2), new Emp(3)]
  //Để lấy được hàm chạy
  const empListFromLocal = JSON.parse(empListJson);
  for (var index = 0; index < empListFromLocal.length; index++) {
    //n1
    const currentEmpl = empListFromLocal[index];
    //new Emp(1) Giống hoàn toàn n1 chỉ khác là có thể dùng được phương thức
    const newEmp = new Employee(
      currentEmpl.id,
      currentEmpl.lastName,
      currentEmpl.firstName,
      currentEmpl.startDate,
      currentEmpl.position
    );
    empList.push(newEmp);
  }

  //Cast chuỗi -> mảng
  // empList = JSON.parse(empListJson);
  // Tạo giao diện danh sách cũ
  renderEmp();
};
getData();

//function 5: Delete emp out ò list
const deleteEmp = function (id) {
  //Xóa phần tử bất kì splice
  //Gọi hàm findID để tìm ID muốn xóa
  const checkID = findEmpByID(id);
  if (checkID !== -1) {
    empList.splice(checkID, 1); //Tìm thấy -> Xóa
    saveData();
    renderEmp(); //Update lại giao diện
  }
};

//function 6: get Update emp
const getUpdateEmp = function (id) {
  //Lấy thông tin muốn chỉnh sửa hiện lên form
  //Tìm emp có trong danh sách
  const checkID = findEmpByID(id);
  if (checkID !== -1) {
    //Đổ data ngược lại form
    document.getElementById("ho").value = empList[checkID].lastName;
    document.getElementById("ten").value = empList[checkID].firstName;
    document.getElementById("msnv").value = empList[checkID].id;
    document.getElementById("datepicker").value = empList[checkID].startDate;
    document.getElementById("chucvu").value = empList[checkID].position;

    //Chặn ô ID không cho edit
    //SetAttribute();
    document.getElementById("msnv").setAttribute("disabled", true);
    //Ẩn button thêm hiện button cập nhật
    document.getElementById("btnAdd").style.display = "none";
    document.getElementById("btnUpdate").style.display = "block";
  }
};

//function 7: update empl
const updateEmpl = function () {
  //1. Lấy thông tin mới sửa từ form (dom)
  const lastName = document.getElementById("ho").value;
  const firstName = document.getElementById("ten").value;
  const id = document.getElementById("msnv").value;
  const startDate = document.getElementById("datepicker").value;
  const position = document.getElementById("chucvu").value;
  //2. id vẫn như cũ, tìm vị trí của nv có id này trong mảng
  const index = findEmpByID(id);

  if (index !== -1) {
    const updatedEmpl = new Employee(
      id,
      lastName,
      firstName,
      startDate,
      position
    );
    empList[index] = updatedEmpl;
    renderEmp();
    //ẩn nút cập nhật, hiện lại nút add
    document.getElementById("btnAdd").style.display = "block";
    document.getElementById("btnUpdate").style.display = "none";
    // mở disabled của ô msnv
    document.getElementById("msnv").removeAttribute("disabled");
    // trigger nút reset nhấn để clear form
    document.getElementById("btnReset").click();
  }
  //3. tiến hành update
  //3.1 emplList[index].lastName =  lastName mới
  //3.2 tạo ra 1 đối tượng NV mới từ info mới: emplList[index] = employeeMoiUpdate
};

//function 8: tìm nhân viên theo mã hoặc theo tên
const findEmpl = function () {
  const foundedEmpl = [];
  //1. Lấy keyword người dùng nhập vào (dom)
  const keyword = document
    .getElementById("txtSearch")
    .value.trim()
    .toLowerCase();
  //2. tìm theo mã: lập for , kiểm tra từng nhân viên trong mảng, có nhân viên nào
  //   có id giống với keyword => push NV đó vào foundedEmpl
  for (var i = 0; i < empList.length; i++) {
    const currentEmp = empList[i];
    var fullName = currentEmp.lastName + " " + currentEmp.firstName;

    fullName = fullName.toLowerCase();

    if (currentEmp.id === keyword) {
      foundedEmpl.push(currentEmp);
      break;
    }
    //indexOf trả về -1 => keyword không nằm trong fullname và ngược lại
    if (fullName.indexOf(keyword) !== -1) {
      foundedEmpl.push(currentEmp);
    }
  }

  //3. console.log(foundedEmpl)
  renderEmp(foundedEmpl);
};

//------VALIDATION FUNCTION (hàm kiểm tra input)------------------------

const checkRequired = function (id, errorId, message) {
  const value = document.getElementById(id).value;
  // if(value !== '')
  //dive to If() when value !== "", null, 0, undefined, false
  if (value) {
    //nhảy vào đây nghĩa là value !== "" => người dùng đã nhập => true
    document.getElementById(errorId).innerHTML = "";
    return true;
  }
  //nhảy xuống đây nghĩa value === "" => người dùng chưa nhập => false
  document.getElementById(errorId).innerHTML = message;
  return false;
};

